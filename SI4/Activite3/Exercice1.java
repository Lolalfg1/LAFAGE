import java.util.Scanner;
public class Exercice1 {
/*
 Variable a entier
 début
      Afficher "saisir un nombre"
      Saisir a
      Si((a%2)=0)
      Alors afficher a, "est pair"
      Sinon 
      Afficher a, "est impair"
      Fin si
      Fin
 */
	public static void main(String[] args) {
		Scanner sc=new Scanner(System.in);
		System.out.println("Veuillez saisir un nombre");
		int a=sc.nextInt();
		if((a%2)==0){
			System.out.println(a+"est pair");
		}else{
			System.out.println(a+"est impair");
		}
	}

}
