package farma;

public class Region {

    private String regCode;
    private String secCode;
    private String regNom;


    public String getRegCode() {
        return regCode;
    }

    public void setRegCode(String regCode) {
        this.regCode = regCode;
    }

    public String getSecCode() {
        return secCode;
    }

    public void setSecCode(String secCode) {
        this.secCode = secCode;
    }

    public String getRegNom() {
        return regNom;
    }

    public void setRegNom(String regNom) {
        this.regNom = regNom;
    }

    public Region(String regCode, String secCode, String regNom) {
        this.regCode = regCode;
        this.secCode = secCode;
        this.regNom = regNom;
    }

    @Override
    public String toString() {
        return "Region{" +
                "regCode='" + regCode + '\'' +
                ", secCode='" + secCode + '\'' +
                ", regNom='" + regNom + '\'' +
                '}';
    }

    public Region() {
    }
}