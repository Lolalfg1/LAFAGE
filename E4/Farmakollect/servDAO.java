package farma;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.sql.PreparedStatement;

public class servDAO {
    public static Connection getConnection() {
        Connection conn = null;


        try {

            Class.forName("com.mysql.jdbc.Driver");

            System.out.println("Oki");

            String url = "jdbc:mysql://neotri.intern/lafagl?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

            String user = "lafagl";

            String passwd = "axel27092014expectopatronum";

            conn = DriverManager.getConnection(url, user, passwd);



        } catch (Exception e) {

            e.printStackTrace();

        }

        return conn;


    }

    //Methode Specialite
    public static List<Specialite> getAllUsers(){

        ArrayList<Specialite> userList = new ArrayList<Specialite>();
        try{
            Connection conn = getConnection();
            Statement statement = conn.createStatement();

            ResultSet resultat = statement.executeQuery("SELECT * FROM lafagl.specialite;");

            while( resultat.next() ){
                Specialite user1 = new Specialite();
                user1.setTypCode(resultat.getString("TYP_CODE"));
                user1.setTypLibelle(resultat.getString("TYP_LIBELLE"));
                user1.setTypLieu(resultat.getString("TYP_LIEU"));
                userList.add(user1);
            }

        }catch(Exception e){

            e.printStackTrace();

        }

        return userList;

    }

    //Methode Medicament
    public static List<Medicament> getAllMedicament(){

        ArrayList<Medicament> userList1 = new ArrayList<Medicament>();
        try{
            Connection conn = getConnection();
            Statement statement = conn.createStatement();

            ResultSet resultat = statement.executeQuery("SELECT * FROM lafagl.medicaments;");

            while( resultat.next() ){
                Medicament user2 = new Medicament();
                user2.setMedDepotLegal(resultat.getString("MED_DEPOTLEGAL"));
                user2.setMedNomCommercial(resultat.getString("MED_NOMCOMMERCIAL"));
                user2.setFam(FamilleDAO.getFamilleByCode(resultat.getString("FAM_CODE")));
                user2.setMedComposition(resultat.getString("MED_COMPOSITION"));
                user2.setMedEffets(resultat.getString("MED_EFFETS"));
                user2.setMedContreindic(resultat.getString("MED_CONTREINDIC"));
                user2.setMedPrix(resultat.getInt("MED_PRIX"));
                userList1.add(user2);
            }

        }catch(Exception e){

            e.printStackTrace();

        }

        return userList1;

    }


    //Methode Praticiens
    public static List<Praticien> getAllPraticien(){

        ArrayList<Praticien> userList2 = new ArrayList<Praticien>();
        try{
            Connection conn = getConnection();
            Statement statement = conn.createStatement();

            ResultSet resultat = statement.executeQuery("SELECT * FROM lafagl.praticiens;");

            while( resultat.next() ){
                Praticien user3 = new Praticien();
                user3.setPraNum(resultat.getInt("PRA_NUM"));
                user3.setPraNom(resultat.getString("PRA_NOM"));
                user3.setPraPrenom(resultat.getString("PRA_PRENOM"));
                user3.setPraAdresse(resultat.getString("PRA_ADRESSE"));
                user3.setCp(resultat.getInt("PRA_CP"));
                user3.setPraVille(resultat.getString("PRA_VILLE"));
                user3.setPraCoefNotoriete(resultat.getString("PRA_COEFNOTORIETE"));
                user3.setTyp(SpecialiteDAO.getSpecialiteByCode(resultat.getString("TYP_CODE")));
                userList2.add(user3);
            }

        }catch(Exception e){

            e.printStackTrace();

        }

        return userList2;

    }

    //Methode region
    public static List<Region> getAllRegion(){

        ArrayList<Region> userList4 = new ArrayList<Region>();
        try{
            Connection conn = getConnection();
            Statement statement = conn.createStatement();

            ResultSet resultat = statement.executeQuery("SELECT * FROM lafagl.region;");

            while( resultat.next() ){
                Region user4 = new Region();
                user4.setRegCode(resultat.getString("REG_CODE"));
                user4.setSecCode(resultat.getString("SEC_CODE"));
                user4.setRegNom(resultat.getString("REG_NOM"));
                userList4.add(user4);
            }


        }catch(Exception e){

            e.printStackTrace();

        }

        return  userList4;

    }

    //Methode Famille
    public static List<Famille> getAllFamille(){

        ArrayList<Famille> userList5 = new ArrayList<Famille>();
        try{
            Connection conn = getConnection();
            Statement statement = conn.createStatement();

            ResultSet resultat = statement.executeQuery("SELECT * FROM lafagl.familles;");

            while( resultat.next() ){
                Famille user5 = new Famille();
                user5.setFamCode(resultat.getString("FAM_CODE"));
                user5.setLibelle(resultat.getString("FAM_LIBELLE"));
                userList5.add(user5);
            }


        }catch(Exception e){

            e.printStackTrace();

        }

        return  userList5;

    }


    //Methode POST region
    public static void insReg (String regionNom, String regionCode, String regionSec){

        try{
            Connection conn = getConnection();
            PreparedStatement preparedStatement = conn.prepareStatement("INSERT INTO lafagl.region (REG_CODE, SEC_CODE, REG_NOM) values (?, ?, ?);");

                preparedStatement.setString(1, regionNom);
                preparedStatement.setString(2, regionCode);
                preparedStatement.setString(3, regionSec);
                preparedStatement.executeUpdate();


        }catch(Exception e){

            e.printStackTrace();

        }



    }








        }