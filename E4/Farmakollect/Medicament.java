package farma;

public class Medicament {

    private String medDepotLegal;
    private String medNomCommercial;
    private Famille fam;
    private String medComposition;
    private String medEffets;
    private String medContreindic;
    private int medPrix;


    public String getMedDepotLegal() {
        return medDepotLegal;
    }

    public void setMedDepotLegal(String medDepotLegal) {
        this.medDepotLegal = medDepotLegal;
    }

    public String getMedNomCommercial() {
        return medNomCommercial;
    }

    public void setMedNomCommercial(String medNomCommercial) {
        this.medNomCommercial = medNomCommercial;
    }

    public Famille getFam() {
        return fam;
    }

    public void setFam(Famille fam) {
        this.fam = fam;
    }

    public String getMedComposition() {
        return medComposition;
    }

    public void setMedComposition(String medComposition) {
        this.medComposition = medComposition;
    }

    public String getMedEffets() {
        return medEffets;
    }

    public void setMedEffets(String medEffets) {
        this.medEffets = medEffets;
    }

    public String getMedContreindic() {
        return medContreindic;
    }

    public void setMedContreindic(String medContreindic) {
        this.medContreindic = medContreindic;
    }

    public int getMedPrix() {
        return medPrix;
    }

    public void setMedPrix(int medPrix) {
        this.medPrix = medPrix;
    }

    public Medicament(String medDepotLegal, String medNomCommercial, Famille fam, String medComposition, String medEffets, String medContreindic, int medPrix) {
        this.medDepotLegal = medDepotLegal;
        this.medNomCommercial = medNomCommercial;
        this.fam = fam;
        this.medComposition = medComposition;
        this.medEffets = medEffets;
        this.medContreindic = medContreindic;
        this.medPrix = medPrix;
    }

    @Override
    public String toString() {
        return "Medicament{" +
                "medDepotLegal='" + medDepotLegal + '\'' +
                ", medNomCommercial='" + medNomCommercial + '\'' +
                ", fam=" + fam +
                ", medComposition='" + medComposition + '\'' +
                ", medEffets='" + medEffets + '\'' +
                ", medContreindic='" + medContreindic + '\'' +
                ", medPrix='" + medPrix + '\'' +
                '}';
    }

    public Medicament(){

    }
}
