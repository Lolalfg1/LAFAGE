package farma;

public class Famille {

    private String famCode;
    private String Libelle;

    public String getFamCode() {
        return famCode;
    }

    public void setFamCode(String famCode) {
        this.famCode = famCode;
    }

    public String getLibelle() {
        return Libelle;
    }

    public void setLibelle(String libelle) {
        Libelle = libelle;
    }

    public Famille(String famCode, String libelle) {
        this.famCode = famCode;
        Libelle = libelle;
    }

    @Override
    public String toString() {
        return "Famille{" +
                "famCode='" + famCode + '\'' +
                ", Libelle='" + Libelle + '\'' +
                '}';
    }

    public Famille() {
    }
}