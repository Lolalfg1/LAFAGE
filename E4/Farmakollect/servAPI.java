package farma;

import com.fasterxml.jackson.databind.ObjectMapper;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.FormParam;
import javax.ws.rs.POST;
import java.sql.*;


@Path("/farm")
public class servAPI {

    //Methode specialites
            @GET
            @Path("/specialites")
            @Produces(MediaType.APPLICATION_JSON)
            public String getFarma() {
                //Affiche les specialites
                ObjectMapper mapper = new ObjectMapper();
                String jsonInString = null;
                try {
                    jsonInString = mapper.writeValueAsString(servDAO.getAllUsers());
                } catch (Exception e) {
                    return e.getMessage();
                }
                return jsonInString;
            }

    //Methode medicaments
    @GET
    @Path("/medicaments")
    @Produces(MediaType.APPLICATION_JSON)
    public String getFarmak() {
        //Affiche les medicaments
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        try {
            jsonInString = mapper.writeValueAsString(servDAO.getAllMedicament());
        } catch (Exception e) {
            return e.getMessage();
        }
        return jsonInString;
    }

    //Methode praticiens
    @GET
    @Path("/praticiens")
    @Produces(MediaType.APPLICATION_JSON)
    public String getFarmako() {
        //Affiche les medicaments
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        try {
            jsonInString = mapper.writeValueAsString(servDAO.getAllPraticien());
        } catch (Exception e) {
            return e.getMessage();
        }
        return jsonInString;
    }

    //Methode region
    @GET
    @Path("/region")
    @Produces(MediaType.APPLICATION_JSON)
    public String getFarmakol() {
        //Affiche les medicaments
        ObjectMapper mapper = new ObjectMapper();
         String jsonInString = null;
        try {
            jsonInString = mapper.writeValueAsString(servDAO.getAllRegion());
        } catch (Exception e) {
            return e.getMessage();
        }
        return jsonInString;

    }

    //Methode famille
    @GET
    @Path("/famille")
    @Produces(MediaType.APPLICATION_JSON)
    public String getFarmakoll() {
        //Affiche les medicaments
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        try {
            jsonInString = mapper.writeValueAsString(servDAO.getAllFamille());
        } catch (Exception e) {
            return e.getMessage();
        }
        return jsonInString;

    }


    //Methode POST REGION
    @POST
    @Path("/insregion")
    @Produces(MediaType.TEXT_PLAIN)
    public void addRegion(@FormParam("regCode") String regCode, @FormParam("secCode") String secCode, @FormParam("regNom") String regNom) {
        servDAO.insReg(regCode, secCode, regNom);
    }







}
