
public class Specialite {

    private String typCode;
    private String typLibelle;
    private String typLieu;

    public String getTypCode() {
        return typCode;
    }

    public void setTypCode(String typCode) {
        this.typCode = typCode;
    }

    public String getTypLibelle() {
        return typLibelle;
    }

    public void setTypLibelle(String typLibelle) {
        this.typLibelle = typLibelle;
    }

    public String getTypLieu() {
        return typLieu;
    }

    public void setTypLieu(String typLieu) {
        this.typLieu = typLieu;
    }

    public Specialite(String typCode, String typLibelle, String typLieu) {
        this.typCode = typCode;
        this.typLibelle = typLibelle;
        this.typLieu = typLieu;
    }

    @Override
    public String toString() {
        return "Specialite{" +
                "typCode='" + typCode + '\'' +
                ", typLibelle='" + typLibelle + '\'' +
                ", typLieu='" + typLieu + '\'' +
                '}';
    }

    public Specialite() {
    }
}