package farma;

public class Praticien {

    private int praNum;
    private String praNom;
    private String praPrenom;
    private String praAdresse;
    private int cp;
    private String praVille;
    private String praCoefNotoriete;
    private Specialite typ;


    public int getPraNum() {
        return praNum;
    }

    public void setPraNum(int praNum) {
        this.praNum = praNum;
    }

    public String getPraNom() {
        return praNom;
    }

    public void setPraNom(String praNom) {
        this.praNom = praNom;
    }

    public String getPraPrenom() {
        return praPrenom;
    }

    public void setPraPrenom(String praPrenom) {
        this.praPrenom = praPrenom;
    }

    public String getPraAdresse() {
        return praAdresse;
    }

    public void setPraAdresse(String praAdresse) {
        this.praAdresse = praAdresse;
    }

    public int getCp() {
        return cp;
    }

    public void setCp(int cp) {
        this.cp = cp;
    }

    public String getPraVille() {
        return praVille;
    }

    public void setPraVille(String praVille) {
        this.praVille = praVille;
    }

    public String getPraCoefNotoriete() {
        return praCoefNotoriete;
    }

    public void setPraCoefNotoriete(String praCoefNotoriete) {
        this.praCoefNotoriete = praCoefNotoriete;
    }

    public Specialite getTyp() {
        return typ;
    }

    public void setTyp(Specialite typ) {
        this.typ = typ;
    }

    public Praticien(int praNum, String praNom, String praPrenom, String praAdresse, int cp, String praVille, String praCoefNotoriete, Specialite typ) {
        this.praNum = praNum;
        this.praNom = praNom;
        this.praPrenom = praPrenom;
        this.praAdresse = praAdresse;
        this.cp = cp;
        this.praVille = praVille;
        this.praCoefNotoriete = praCoefNotoriete;
        this.typ = typ;
    }

    @Override
    public String toString() {
        return "Praticien{" +
                "praNum=" + praNum +
                ", praNom='" + praNom + '\'' +
                ", praPrenom='" + praPrenom + '\'' +
                ", praAdresse='" + praAdresse + '\'' +
                ", cp=" + cp +
                ", praVille='" + praVille + '\'' +
                ", praCoefNotoriete='" + praCoefNotoriete + '\'' +
                ", typ=" + typ +
                '}';
    }

    public Praticien() {
    }
}

