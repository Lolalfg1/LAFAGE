package edu.TD3;

public class Chemise extends Vete {
	private double couTaille;
	private double longueurManche;
	

	
	public Chemise(String l, String c, String m, String n, double p, double couTaille, double longueurManche){
		super(l, c, m, n, p);
		this.couTaille=couTaille;
		this.longueurManche=longueurManche;
	}
	
	
	public double getChemise(double couTaille, double longueurManche){
		return this.couTaille;
	}
	
	
	
	public void setChemise(double couTaille, double longueurManche){
		this.couTaille=couTaille;
		this.longueurManche=longueurManche;
	}
	
	
	
	public double getCouTaille() {
		return couTaille;
	}
	
	
	
	public void setCouTaille(double couTaille) {
		this.couTaille = couTaille;
	}
	
	
	
	public double getLongueurManche() {
		return longueurManche;
	}
	
	
	
	public void setLongueurManche(double longueurManche) {
		this.longueurManche = longueurManche;
	}
	
	
	
	public static void main (String[]args){ 
		Chemise chemise1 = new Chemise ("Chemise", "blanc", "Zara", "Printemps", 29.99, 50, 40);
		System.out.println(chemise1);
		Chemise chemise2 = new Chemise ("Chemise", "bleu", "Zara", "Hiver", 22, 50, 40);
		System.out.println(chemise2);
		Chemise chemise3 = new Chemise ("Chemise", "Noir", "Zara", "Printemps", 25.99, 50, 40);
		System.out.println(chemise3);
		
	}
	
	

}
