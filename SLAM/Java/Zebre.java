
public class Zebre extends Animal{
	
	static int rayure = 10;
	
	
	
	public Zebre(String n, String t, double p, int rayure){
		super(n, t, p);
		
	}
	
	public int getRayure() {
		return rayure;
	}

	public void setRayure(int rayure) {
		this.rayure = rayure;
	}
	
	
	
	
	void crie(){
		System.out.println("Je hennis");
	}
	
	
	void courir(){
		System.out.println("Je suis un zebre et je cours");
	}
	
	

	public String toString(){
		return super.toString()+ " J'ai "+this.getRayure()+" rayure";
	}



}
