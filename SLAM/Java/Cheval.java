

public class Cheval extends Animal{
	private String couleur;
	
	
	
	public Cheval(String n, String t, double p, String c){
		super(n, t, p);
		this.couleur=c;
		
		
	}

	public String getCouleur() {
		return couleur;
	}

	public void setCouleur(String couleur) {
		this.couleur = couleur;
	}
	
	
	
	@Override
	void crie(){
		System.out.println("Je hennis");
	}
	
	void courir(){
		System.out.println("Je suis un cheval et je cours");
	}

	
}
