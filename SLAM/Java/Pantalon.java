package edu.TD3;


public class Pantalon extends Vete {

	private double longueurJambe;
	private double tourtaille;

	
	
	public Pantalon(String l, String c, String m, String n, double p, double j, double t){
		super(l, c, m, n, p);
		this.longueurJambe=j;
		this.tourtaille=t;
		
	}
	
	public double getPantalon(double j, double t){
		return this.longueurJambe;
		
	}
	
	
	
	public double getTourtaille() {
		return tourtaille;
	}
	
	

	public void setTourtaille(double tourtaille) {
		this.tourtaille = tourtaille;
	}
	
	
	public void setPantalon(double j, double t){
		this.longueurJambe=j;
		this.tourtaille=t;
		
	}
	
	

	public double getLongueurJambe() {
		return longueurJambe;
	}
	
	

	public void setLongueurJambe(double longueurJambe) {
		this.longueurJambe = longueurJambe;
	}
	
	
	
	public static void main (String[]args){
		Pantalon pantalon1 = new Pantalon ("Pantalon", "Noir", "Undiz", "Printemps", 16.95, 70, 54);
		System.out.println(pantalon1);
		Pantalon pantalon2 = new Pantalon ("Pantalon", "Bleu", "Mango", "Hiver", 39.99, 70, 54);
		System.out.println(pantalon2);
		Pantalon pantalon3 = new Pantalon ("Pantalon", "Gris", "Mango", "Printemps", 39.99, 70, 54);
		System.out.println(pantalon3);
		
	}
	
	
}