package application;


import javafx.geometry.Insets;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.Button;
import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.scene.paint.Color;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.TextField;
public class JavaFXConnexion extends Application{
	public static void main(String[]args){
		launch(args);
	}
	
	public void start(Stage primaryStage){
		// titre de la fentre
		primaryStage.setTitle("JavaFX Welcome!");
		GridPane grid = new GridPane();
		//grid.setGridLinesVisible(true) //voir les ligne du tableau;
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));
		//Gros titre 
		Text scenetitle = new Text("Welcome");
		scenetitle.setFont(Font.font("Apple Chancery", FontWeight.NORMAL, 70));
		grid.add(scenetitle, 0, 0, 2, 1);
		//nom d'utilisateur
		Label userName = new Label( "User Name:");
		grid.add(userName, 0, 1);
		TextField userTextField = new TextField();
		grid.add(userTextField, 1, 1);
		//mot de passe
		Label pw = new Label("Password:");
		grid.add(pw, 0, 2);
		//creation de la variable mot de passe
		PasswordField pwBox = new PasswordField();
		grid.add(pwBox, 1, 2);
		//creation du bouton
		Button btn = new Button( "Sign in" );
		HBox hbBtn = new HBox(10);
		hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		hbBtn.getChildren().add(btn);
		grid.add(hbBtn, 1, 4);
		final Text actiontarget = new Text();
		grid.add(actiontarget, 1, 6);
		btn.setOnAction( new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				actiontarget.setFill(Color.FIREBRICK);
				actiontarget.setId( "actiontarget" );
				actiontarget.setText("Sign in button pressed");
			}
		});
		Scene scene = new Scene(grid, 300, 275);
		primaryStage.setScene(scene);
		primaryStage.show();
	}
}
