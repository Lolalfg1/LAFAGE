

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.text.Text;
import javafx.scene.control.TextField;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;






public class Activite7Etape4FX extends Application {

	@Override
	public void start(Stage primaryStage) {

		primaryStage.setTitle("Appli JavaFX");

		GridPane grid = new GridPane(); 
		grid.setHgap(10); 
		grid.setVgap(10); 
		grid.setPadding(new Insets(1, 1, 1, 1));

		//Grid nombre
		GridPane grid1 = new GridPane(); 
		grid1.setId("grid1");
		grid1.setHgap(10); 
		grid1.setVgap(10); 
		grid1.setPadding(new Insets(1, 1, 1, 1));
		

		//Grid rectangle
		GridPane grid2 = new GridPane();
		grid2.setId("grid2");
		grid2.setHgap(10); 
		grid2.setVgap(10); 
		grid2.setPadding(new Insets(1, 1, 1, 1));
		//grid2.setAlignment(Pos.CENTER);
		
		//Label Rectangle
		final Label Resultat = new Label();
		Resultat.setId("Rectangle");
		Resultat.setMinSize(80,  40);;
		grid2.add(Resultat, 1, 0);
		

		//Grid Bouton
		GridPane gridbouton = new GridPane(); 
		gridbouton.setHgap(10); 
		gridbouton.setVgap(10); 
		gridbouton.setPadding(new Insets(1, 1, 1, 1));

		//Calculatrice
		Text scenetitle = new Text("Calculatrice");
		scenetitle.setId("calculatrice-text");
		//grid.setAlignment(Pos.CENTER);
		grid.add(scenetitle, 0,0); 

		//premier nombre
		Label Nb1 = new Label("1er Nombre:");
		grid1.add(Nb1, 0, 0);
		TextField Nb1TextField = new TextField( );
		grid1.add(Nb1TextField, 1, 0); 

		//deuxième nombre
		Label Nb2 = new Label("2e Nombre:");
		grid1.add(Nb2, 0, 1);
		TextField Nb2TextField = new TextField();
		grid1.add(Nb2TextField, 1, 1);

		//Bouton addition
		Button btnAdd = new Button( "Additionner"); 
		btnAdd.setId("Addition");
		HBox hbBtnAdd = new HBox(3); 
		hbBtnAdd.getChildren().add(btnAdd); 
		gridbouton.add(hbBtnAdd, 2, 0); 
		
		//Bouton Soustraction
		Button btnSous = new Button( "Soustraire"); 
		btnSous.setId("Soustaction");
		HBox hbBtnSous = new HBox(3); 
		hbBtnSous.getChildren().add(btnSous); 
		gridbouton.add(hbBtnSous, 3, 0);
		

		//Bouton multiplication
		Button btnMult = new Button( "Multiplier"); 
		btnMult.setId("Multiplication");
		HBox hbBtnMult = new HBox(3);
		hbBtnMult.getChildren().add(btnMult); 
		gridbouton.add(hbBtnMult, 0, 0);
		

		//Bouton division
		Button btnDiv = new Button( "Diviser"); 
		btnDiv.setId("Division");
		HBox hbBtnDiv = new HBox(3); 
		hbBtnDiv.getChildren().add(btnDiv); 
		gridbouton.add(hbBtnDiv, 1, 0); 
		


		
         //Resultat Addition
		final Text actiontarget1 = new Text();
		grid1.add(actiontarget1, 5, 1);
		btnAdd.setOnAction( new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				
				

				double premierNb1, deuxiemeNb1, total1;
				premierNb1 = Double.parseDouble(Nb1TextField.getText());
				deuxiemeNb1 = Double.parseDouble(Nb2TextField.getText());
				total1 = premierNb1+deuxiemeNb1;
				Resultat.setText(Double.toString(total1));
				
				
				
			}
		});
		
		//Resultat Soustraction
		final Text actiontarget3 = new Text();
		grid1.add(actiontarget3, 5, 1);
		btnSous.setOnAction( new EventHandler<ActionEvent>() { 

			@Override
			public void handle(ActionEvent e) {
				
				

				double premierNb2, deuxiemeNb2, total2;
				premierNb2 = Double.parseDouble(Nb1TextField.getText());
				deuxiemeNb2 = Double.parseDouble(Nb2TextField.getText());
				total2 = premierNb2-deuxiemeNb2;
				Resultat.setText(Double.toString(total2));
			}
		});
		
		//Resultat Multiplication
		final Text actiontarget5 = new Text();
		grid1.add(actiontarget5, 5, 1);
		btnMult.setOnAction( new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				
				

				double premierNb3, deuxiemeNb3, total3;
				premierNb3 = Double.parseDouble(Nb1TextField.getText());
				deuxiemeNb3 = Double.parseDouble(Nb2TextField.getText());
				total3 = premierNb3*deuxiemeNb3;
				Resultat.setText(Double.toString(total3));


			}
		});
		
		//Resultat Division
		final Text actiontarget6 = new Text();
		grid1.add(actiontarget6, 5, 1);
		btnDiv.setOnAction( new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {

				double premierNb4, deuxiemeNb4, total4;
				premierNb4 = Double.parseDouble(Nb1TextField.getText());
				deuxiemeNb4 = Double.parseDouble(Nb2TextField.getText());
				total4 = premierNb4/deuxiemeNb4;
				Resultat.setText(Double.toString(total4));

			}
		});

		grid.add(grid1, 0, 1);
		grid.add(gridbouton, 0, 2);
		grid.add(grid2, 0, 3);

		final Text actiontarget = new Text();
		grid1.add(actiontarget, 0, 0); 

		final Text actiontargetbtn = new Text();
		gridbouton.add(actiontargetbtn, 0,0); 

		final Text actiontarget2 = new Text();
		grid2.add(actiontarget2, 0, 0); 

		Scene scene = new Scene(grid, 500,300);
		primaryStage.setScene(scene);
		scene.getStylesheets().add(Activite7Etape4FX.class. getResource("Activite7Style.css").toExternalForm());
		

		primaryStage.show();
	}
	public static void main(String[] args) {
		launch(args); 
	}
}
