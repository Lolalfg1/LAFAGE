

import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.scene.text.TextAlignment;
import javafx.scene.control.TextField;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;






public class Activite7Etape2FX extends Application {

	@Override
	public void start(Stage primaryStage) {

		primaryStage.setTitle("Appli JavaFX");

		GridPane grid = new GridPane(); 
		grid.setAlignment(Pos.TOP_CENTER);
		grid.setHgap(10); 
		grid.setVgap(10); 
		grid.setPadding(new Insets(1, 1, 1, 1));

		//Grid nombre
		GridPane grid1 = new GridPane(); 
		grid1.setAlignment(Pos.TOP_CENTER);
		grid1.setHgap(10); 
		grid1.setVgap(10); 
		grid1.setPadding(new Insets(1, 1, 1, 1));
		

		//Grid rectangle
		GridPane grid2 = new GridPane(); 
		grid2.setAlignment(Pos.BOTTOM_CENTER);
		grid2.setHgap(10); 
		grid2.setVgap(10); 
		grid2.setPadding(new Insets(1, 1, 1, 1));
		grid2.setAlignment(Pos.CENTER);
		
		//Label Rectangle
		final Label Resultat = new Label();
		Resultat.setMinSize(80,  40);;
		grid2.add(Resultat, 1, 0);
		Resultat.setTextFill(Color.BLACK);
		Resultat.setTextAlignment(TextAlignment.CENTER);
		Resultat.setStyle("-fx-alignment: center; -fx-border-color: black;");

		//Grid Bouton
		GridPane gridbouton = new GridPane(); 
		gridbouton.setAlignment(Pos.BOTTOM_CENTER);
		gridbouton.setHgap(10); 
		gridbouton.setVgap(10); 
		gridbouton.setPadding(new Insets(1, 1, 1, 1));

		//Calculatrice
		Text scenetitle = new Text("Calculatrice");
		scenetitle.setFont(Font.font("Apple Chancery", FontWeight.NORMAL, 70)); 
		grid.setAlignment(Pos.CENTER);
		grid.add(scenetitle, 0,0); 

		//premier nombre
		Label Nb1 = new Label("1er Nombre:");
		grid1.add(Nb1, 0, 0);
		TextField Nb1TextField = new TextField( );
		grid1.add(Nb1TextField, 1, 0); 

		//deuxième nombre
		Label Nb2 = new Label("2e Nombre:");
		grid1.add(Nb2, 0, 1);
		TextField Nb2TextField = new TextField();
		grid1.add(Nb2TextField, 1, 1);

		//Bouton addition
		Button btnAdd = new Button( "Additionner"); 
		HBox hbBtnAdd = new HBox(3);
		hbBtnAdd.setAlignment(Pos.BOTTOM_LEFT); 
		hbBtnAdd.getChildren().add(btnAdd); 
		gridbouton.add(hbBtnAdd, 2, 0); 
		
		//Bouton Soustraction
		Button btnSous = new Button( "Soustraire"); 
		HBox hbBtnSous = new HBox(3);
		hbBtnSous.setAlignment(Pos.BOTTOM_LEFT); 
		hbBtnSous.getChildren().add(btnSous); 
		gridbouton.add(hbBtnSous, 3, 0);
		

		//Bouton multiplication
		Button btnMult = new Button( "Multiplier"); 
		HBox hbBtnMult = new HBox(3);
		hbBtnMult.setAlignment(Pos.BOTTOM_LEFT); 
		hbBtnMult.getChildren().add(btnMult); 
		gridbouton.add(hbBtnMult, 0, 0);
		

		//Bouton division
		Button btnDiv = new Button( "Diviser"); 
		HBox hbBtnDiv = new HBox(3);
		hbBtnDiv.setAlignment(Pos.BOTTOM_LEFT); 
		hbBtnDiv.getChildren().add(btnDiv); 
		gridbouton.add(hbBtnDiv, 1, 0); 
		


		
         //Resultat Addition
		final Text actiontarget1 = new Text();
		grid1.add(actiontarget1, 5, 1);
		btnAdd.setOnAction( new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				
				

				double premierNb1, deuxiemeNb1, total1;
				premierNb1 = Double.parseDouble(Nb1TextField.getText());
				deuxiemeNb1 = Double.parseDouble(Nb2TextField.getText());
				total1 = premierNb1+deuxiemeNb1;
				Resultat.setText(Double.toString(total1));
				
				
				
			}
		});
		
		//Resultat Soustraction
		final Text actiontarget3 = new Text();
		grid1.add(actiontarget3, 5, 1);
		btnSous.setOnAction( new EventHandler<ActionEvent>() { 

			@Override
			public void handle(ActionEvent e) {
				
				

				double premierNb2, deuxiemeNb2, total2;
				premierNb2 = Double.parseDouble(Nb1TextField.getText());
				deuxiemeNb2 = Double.parseDouble(Nb2TextField.getText());
				total2 = premierNb2-deuxiemeNb2;
				Resultat.setText(Double.toString(total2));
			}
		});
		
		//Resultat Multiplication
		final Text actiontarget5 = new Text();
		grid1.add(actiontarget5, 5, 1);
		btnMult.setOnAction( new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {
				
				

				double premierNb3, deuxiemeNb3, total3;
				premierNb3 = Double.parseDouble(Nb1TextField.getText());
				deuxiemeNb3 = Double.parseDouble(Nb2TextField.getText());
				total3 = premierNb3*deuxiemeNb3;
				Resultat.setText(Double.toString(total3));


			}
		});
		
		//Resultat Division
		final Text actiontarget6 = new Text();
		grid1.add(actiontarget6, 5, 1);
		btnDiv.setOnAction( new EventHandler<ActionEvent>() {
			@Override
			public void handle(ActionEvent e) {

				double premierNb4, deuxiemeNb4, total4;
				premierNb4 = Double.parseDouble(Nb1TextField.getText());
				deuxiemeNb4 = Double.parseDouble(Nb2TextField.getText());
				total4 = premierNb4/deuxiemeNb4;
				Resultat.setText(Double.toString(total4));

			}
		});

		grid.add(grid1, 0, 1);
		grid.add(gridbouton, 0, 2);
		grid.add(grid2, 0, 3);

		final Text actiontarget = new Text();
		grid1.add(actiontarget, 0, 0); 

		final Text actiontargetbtn = new Text();
		gridbouton.add(actiontargetbtn, 0,0); 

		final Text actiontarget2 = new Text();
		grid2.add(actiontarget2, 0, 0); 

		Scene scene = new Scene(grid, 500,300, Color.MEDIUMSLATEBLUE);
		primaryStage.setScene(scene);
		grid.setStyle("-fx-background-color: #7B68EE");
		btnAdd.setStyle("-fx-background-color: #0080FF; -fx-border-color: black");
		btnSous.setStyle("-fx-background-color: #0080FF; -fx-border-color: black");
		btnMult.setStyle("-fx-background-color: #0080FF; -fx-border-color: black");
		btnDiv.setStyle("-fx-background-color: #0080FF; -fx-border-color: black");

		primaryStage.show();
	}
	public static void main(String[] args) {
		launch(args); 
	}
}
