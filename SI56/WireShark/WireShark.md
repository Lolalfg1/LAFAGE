***Lola Lafage***
***6 novembre 2017***

# Utilisation de wireshark

# HTTP: Celui qui envoie est 10.0.17.191 et celui qui reçoit est 10.0.2.2
dans la barre d'info nous pouvons voir "CONNECT fr.wikipedia.org:443 HTTP/1.1

# FTP: Celui qui envoie est 10.0.2.2 et celui qui reçoit est 10.0.17.191,
dans la partie info nous pouvons voir 3128->49532. C'est un système privé, il n'y a pas d'identifiant anonyme. Les connexion IPv6 peuvent aussi être présentés sur un serveur.


