#Lola Lafage
#3 Février 2017
#serveur Multithread



import socket 
import threading



class clientThread(threading.Thread): 
    
    def init( self, ip, port, clientsocket): 
        threading.Thread.init( self)
        self.ip = ip
        self.port = port 
        self.clientsocket = clientsocket
        print( "[+] Nouveau thread pour %s %s" % (self.ip, self.port, ))

    def run( self): 
        while True : 
            print( "Connection de %s %s" % (self.ip, self.port, ))
            r = self.clientsocket.recv(2048).decode()
            print( "Ouverture du fichier: ", r, "...")
            fp = open(r, 'rb')
            contenu = fp.read()
            if contenu == 'exit':
                break
            print(contenu)
            self.clientsocket.send(contenu)

            print( "Client déconnecté...")


TCP_IP = 'localhost'
TCP_PORT = 1111
BUFFER_SIZE = 20

tcpsock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
tcpsock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
tcpsock.bind((TCP_IP, TCP_PORT))
threads = []

while True:
    tcpsock.listen(10)
    print("En écoute...")
    (clientsocket, (ip, port))= tcpsock.accept()
    newthread = ClientThread(ip, port, clientsocket)
    newthread.start()
    threads.append(newthread)

print("Close")
clientsocket.close()
socket.close()
