***Lola Lafage***
***23 Janvier 2017***


# ***Activité 6: virtualisation***

### Etape 1 et 2:
J'ai dans un premier temps télécharger "virtualBox" sous Mac car c'est
l'ordinateur sur lequel je suis. A la suite de quoi j'ai procédé à son
instalation jusqu'à obtenir un menu.

Puis, il faut cliquer sur "nouvelle" ce qui nous amène sur une fenêtre où il
est écrit "Nom et système d'exploitation" où on doit donner un nom (au choix,
de préférence en rapport avec le type souhaité) puis choisir le type qui
correspond, la version se mettera automatiquement, puis cliquer sur suivant.
Apres il faut aller dans "configuration" qui est le bouton juste à côté de
"nouvelle", une fois entrer il faut aller dans "stackage" puis cliquer sur
 "vide" qui est accompagné d'un petit dvd, à la suite de quoi,
 à droite sous le champ "attributs", il y aura un autre petit dvd, une fois
cliquer dessus, on doit choisir un dossier qui nous a été prealablement gentillement donner par la professeur, puis cliquer sur ok.
Il ne reste plus qu'à lancer le démarage.

Le démarage activé, le telechargement de linux (dans notre cas) sera lancer
il faut cliquer sur "continué" jusqu'a ce qu'on nous demande de choisir un
identifiant et un mot de passe (qui doit être chosit par nous et ne pas être
oublié) ensuite on dit "oui" à tout sauf pour le miroir sur le réseau qui
doit être refusé. Quand le choix du périphérique sera présent, il faudra
choisir ata_vBox. puis appuier sur continué jusqu'à la fin.



### Etape 3:
Pour être connecté à la wifi, il faut aller dans la virtualBox puis dans      
"configuration", puis dans "réseau" et dans le mode d'accès réseau il 
faut choisir "l'accès par pont".


### Etape 4:

Il faut aller dans périférique, puis choisir "insert l'image CD des additions", puis choisir un fichier au choix, ensuite il faut aller dans le terminal en super utilisateur puis on fait "./VBoxLinuxAddition.run, puis "apt update -y", puis "rebbot" pour redémarer l'ordinateur, puis une fois relancer rentrer dans le terminal "ls", puis "cd/media/", puis ls et notre dossier choisit sera là et si l'on rentre par exemple: ("cd sf_Irlande/") on rentrera dans le fichier et on pourra acceder a tout ce qu'il contient.


### Etape 5: 

pour communiquer avec la machine virtuelle, il faut tout d'abord trouver l'adresse IP de la machine virtuelle en tapant "ip -a" ce qui dans mon cas donne 10.0.18.12 a la suite de quoi il faut aller dans le terminal hors de la machine virtuelle donc celui de notre machine et pinger cette adresse ce qui établira une communication avec la machine.





                     


