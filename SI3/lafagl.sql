-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 22 Septembre 2016 à 17:05
-- Version du serveur :  5.5.49-0+deb8u1
-- Version de PHP :  5.6.20-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `lafagl`
--

-- --------------------------------------------------------

--
-- Structure de la table `animal`
--

CREATE TABLE IF NOT EXISTS `animal` (
`numAnimal` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `animal`
--

INSERT INTO `animal` (`numAnimal`, `nom`) VALUES
(1, 'Pompom'),
(2, 'pomponnette'),
(3, 'Jerry'),
(5, 'Azrael');

-- --------------------------------------------------------

--
-- Structure de la table `animal2`
--

CREATE TABLE IF NOT EXISTS `animal2` (
`numAnimal` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `animal2`
--

INSERT INTO `animal2` (`numAnimal`, `nom`) VALUES
(1, 'Pompom'),
(2, 'pomponnette'),
(3, 'Jerry'),
(4, 'Snoopy'),
(5, 'Azrael');

-- --------------------------------------------------------

--
-- Structure de la table `consultation`
--

CREATE TABLE IF NOT EXISTS `consultation` (
  `numeroProprio` int(11) NOT NULL,
  `numeroAnimal` int(11) NOT NULL,
  `dateConsultation` date NOT NULL,
  `raison` varchar(20) NOT NULL,
  `prenomMedecin` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `consultation`
--

INSERT INTO `consultation` (`numeroProprio`, `numeroAnimal`, `dateConsultation`, `raison`, `prenomMedecin`) VALUES
(1, 1, '2013-09-20', ' problème rénal', 'Gabrielle'),
(2, 2, '2013-09-20', ' vaccin', 'Gabrielle'),
(3, 3, '2013-09-03', 'plaie à la patte', 'Gabrielle'),
(4, 4, '2013-09-10', 'troubles du sommeil', 'Pierre'),
(5, 5, '2013-09-01', 'problèmes comporteme', 'Pierre');

-- --------------------------------------------------------

--
-- Structure de la table `consultation2`
--

CREATE TABLE IF NOT EXISTS `consultation2` (
`numeroC` int(11) NOT NULL,
  `numeroProprio` int(11) NOT NULL,
  `numeroAnimal` int(11) NOT NULL,
  `dateConsultation` date NOT NULL,
  `raison` varchar(20) NOT NULL,
  `prenomMedecin` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `consultation2`
--

INSERT INTO `consultation2` (`numeroC`, `numeroProprio`, `numeroAnimal`, `dateConsultation`, `raison`, `prenomMedecin`) VALUES
(16, 1, 1, '2013-09-20', 'problème rénal', 'Gabrielle'),
(17, 2, 2, '2013-09-20', 'vaccin', 'Gabrielle'),
(18, 3, 3, '2013-09-03', 'plaie à la patte', 'Gabrielle'),
(20, 4, 4, '2013-09-10', 'troubles du sommeil', 'Pierre'),
(21, 5, 5, '2013-09-01', 'probleme comporteme', 'Pierre');

-- --------------------------------------------------------

--
-- Structure de la table `medecin`
--

CREATE TABLE IF NOT EXISTS `medecin` (
`numMedecin` int(11) NOT NULL,
  `nomMedecin` varchar(20) NOT NULL,
  `prenomMedecin` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `medecin`
--

INSERT INTO `medecin` (`numMedecin`, `nomMedecin`, `prenomMedecin`) VALUES
(1, 'Burel', 'Gabrielle'),
(2, 'Burel', 'Pierre'),
(3, 'Parker', 'Peter'),
(4, 'Winchester', 'Dean'),
(5, 'Granger', 'Hermione');

-- --------------------------------------------------------

--
-- Structure de la table `proprietaire`
--

CREATE TABLE IF NOT EXISTS `proprietaire` (
`numProprio` int(11) NOT NULL,
  `nomProprio` varchar(20) NOT NULL,
  `prenomProprio` varchar(20) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `proprietaire`
--

INSERT INTO `proprietaire` (`numProprio`, `nomProprio`, `prenomProprio`) VALUES
(1, 'Castagnier', 'Aimable'),
(2, 'Castagnier', 'Aimable'),
(3, 'Aimée', 'Madeleine'),
(4, ' Brown', 'Charly'),
(5, ' Gargamel', 'Jules');

-- --------------------------------------------------------

--
-- Structure de la table `proprietaire2`
--

CREATE TABLE IF NOT EXISTS `proprietaire2` (
`numProprio` int(11) NOT NULL,
  `nomProprio` varchar(20) NOT NULL,
  `prenomProprio` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `proprietaire2`
--

INSERT INTO `proprietaire2` (`numProprio`, `nomProprio`, `prenomProprio`) VALUES
(1, 'Castagnier', 'Aimable'),
(2, 'Castagnier', 'Aimable'),
(3, 'Aimée', 'Madeleine'),
(4, ' Brown', 'Charly'),
(5, ' Gargamel', 'Jules');

--
-- Index pour les tables exportées
--

--
-- Index pour la table `animal`
--
ALTER TABLE `animal`
 ADD PRIMARY KEY (`numAnimal`);

--
-- Index pour la table `animal2`
--
ALTER TABLE `animal2`
 ADD PRIMARY KEY (`numAnimal`);

--
-- Index pour la table `consultation`
--
ALTER TABLE `consultation`
 ADD PRIMARY KEY (`numeroProprio`,`numeroAnimal`), ADD KEY `consultation_ibfk_2` (`prenomMedecin`);

--
-- Index pour la table `consultation2`
--
ALTER TABLE `consultation2`
 ADD PRIMARY KEY (`numeroC`), ADD KEY `numeroAnimal` (`numeroAnimal`), ADD KEY `numeroProprio` (`numeroProprio`);

--
-- Index pour la table `medecin`
--
ALTER TABLE `medecin`
 ADD PRIMARY KEY (`numMedecin`);

--
-- Index pour la table `proprietaire`
--
ALTER TABLE `proprietaire`
 ADD PRIMARY KEY (`numProprio`);

--
-- Index pour la table `proprietaire2`
--
ALTER TABLE `proprietaire2`
 ADD PRIMARY KEY (`numProprio`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `animal`
--
ALTER TABLE `animal`
MODIFY `numAnimal` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `animal2`
--
ALTER TABLE `animal2`
MODIFY `numAnimal` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `consultation2`
--
ALTER TABLE `consultation2`
MODIFY `numeroC` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `medecin`
--
ALTER TABLE `medecin`
MODIFY `numMedecin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `proprietaire`
--
ALTER TABLE `proprietaire`
MODIFY `numProprio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `proprietaire2`
--
ALTER TABLE `proprietaire2`
MODIFY `numProprio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `consultation2`
--
ALTER TABLE `consultation2`
ADD CONSTRAINT `consultation2_ibfk_2` FOREIGN KEY (`numeroAnimal`) REFERENCES `animal2` (`numAnimal`),
ADD CONSTRAINT `consultation2_ibfk_1` FOREIGN KEY (`numeroProprio`) REFERENCES `proprietaire2` (`numProprio`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
