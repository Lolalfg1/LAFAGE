-- phpMyAdmin SQL Dump
-- version 4.2.12deb2+deb8u1
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Jeu 24 Novembre 2016 à 15:08
-- Version du serveur :  5.5.49-0+deb8u1
-- Version de PHP :  5.6.20-0+deb8u1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `lafagl`
--

-- --------------------------------------------------------

--
-- Structure de la table `animal`
--

CREATE TABLE IF NOT EXISTS `animal` (
`numAnimal` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `animal`
--

INSERT INTO `animal` (`numAnimal`, `nom`) VALUES
(1, 'Pompom'),
(2, 'pomponnette'),
(3, 'Jerry'),
(5, 'Azrael');

-- --------------------------------------------------------

--
-- Structure de la table `animal2`
--

CREATE TABLE IF NOT EXISTS `animal2` (
`numAnimal` int(11) NOT NULL,
  `nom` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `animal2`
--

INSERT INTO `animal2` (`numAnimal`, `nom`) VALUES
(1, 'Pompom'),
(2, 'pomponnette'),
(3, 'Jerry'),
(4, 'Snoopy'),
(5, 'Azrael');

-- --------------------------------------------------------

--
-- Structure de la table `article`
--

CREATE TABLE IF NOT EXISTS `article` (
  `refprod` varchar(20) NOT NULL DEFAULT '',
  `designation` varchar(30) DEFAULT NULL,
  `couleur` varchar(30) DEFAULT NULL,
  `dim` varchar(20) DEFAULT NULL,
  `prixHt` int(11) DEFAULT NULL,
  `NumColl` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `article`
--

INSERT INTO `article` (`refprod`, `designation`, `couleur`, `dim`, `prixHt`, `NumColl`) VALUES
('A12', 'Chaise longue', 'Marine', '120X60', 90, 1),
('A14', 'Serviette de bain', 'Orangé', '130X80', 65, 2),
('A15', 'Cousin', 'Paille', '30X30', 12, 2);

-- --------------------------------------------------------

--
-- Structure de la table `ARTICLE`
--

CREATE TABLE IF NOT EXISTS `ARTICLE` (
  `Refart` varchar(20) NOT NULL DEFAULT '',
  `NomArt` varchar(40) NOT NULL,
  `Qte` int(10) DEFAULT NULL,
  `PrixUnitaire` int(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `ARTICLE`
--

INSERT INTO `ARTICLE` (`Refart`, `NomArt`, `Qte`, `PrixUnitaire`) VALUES
('FO355', 'Cafetière', 4, 7),
('FO561', 'Poele', 1, 18),
('FO863', 'Mixeur', 2, 2),
('GP651', 'Grille Pain Jaune', 1, 25),
('PG544', 'Micronde', 3, 5),
('PG578', 'Aspirateur', 1, 5),
('PQ345', 'Casserole', 2, 12);

-- --------------------------------------------------------

--
-- Structure de la table `client`
--

CREATE TABLE IF NOT EXISTS `client` (
`idCli` int(11) NOT NULL,
  `nomCli` varchar(20) NOT NULL,
  `prenomCli` varchar(20) NOT NULL,
  `adresse` varchar(20) NOT NULL,
  `dateNaiss` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `client`
--

INSERT INTO `client` (`idCli`, `nomCli`, `prenomCli`, `adresse`, `dateNaiss`) VALUES
(1, 'Harry', 'Charles', '12 rue du prÃ©', '1987-11-12'),
(2, 'Dupond', 'John', '13 rue du parc', '1987-11-26'),
(3, 'Dupo', 'Winnie', '19 rue du parc', '1995-04-12'),
(4, 'Dupo', 'Jennifer', '12 rue du parc', '1987-11-01'),
(5, 'Dodo', 'Dorianne', '28 rue du pommier', '2011-11-16'),
(6, 'Binouf', 'Baptiste', '19 rue du chene', '2011-11-18'),
(7, 'Boria', 'Barry', '29 rue du pommier', '2011-11-23'),
(8, 'Bibi', 'Foque', '29 rue du mistral', '2011-11-18'),
(9, 'Wang', 'Goth', '18 rue du prunus', '2011-10-12'),
(10, 'frada', 'Frederic', '27 rue du chene', '2011-10-13'),
(11, 'Faudiere', 'frederic', '19 rue du prunus', '2011-11-25');

-- --------------------------------------------------------

--
-- Structure de la table `CLIENT`
--

CREATE TABLE IF NOT EXISTS `CLIENT` (
  `NumCli` int(20) NOT NULL DEFAULT '0',
  `Nom` varchar(20) DEFAULT NULL,
  `Prenom` varchar(20) DEFAULT NULL,
  `Adresse` varchar(30) DEFAULT NULL,
  `NumTel` int(10) DEFAULT NULL,
  `Ville` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `CLIENT`
--

INSERT INTO `CLIENT` (`NumCli`, `Nom`, `Prenom`, `Adresse`, `NumTel`, `Ville`) VALUES
(34067, 'JACQUIN', 'Marie-Jeanne', '3 rue du puits 95800 ', 135353535, 'Baillon'),
(35165, 'FOUQUET', 'Reine-Marie', '3 rue de la forêt ', 450505050, 'Marilly'),
(45800, 'Winchester', 'Dean', '2 rue de la voiture ', 122222322, 'Champagne'),
(46840, 'Fraser', 'Jamie', '5 rue de la coline ', 655555252, 'Presles');

-- --------------------------------------------------------

--
-- Structure de la table `Collection`
--

CREATE TABLE IF NOT EXISTS `Collection` (
  `NumColl` int(11) NOT NULL,
  `DateLancement` date DEFAULT NULL,
  `NomColl` varchar(30) DEFAULT NULL,
  `Harmonie` varchar(30) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `Collection`
--

INSERT INTO `Collection` (`NumColl`, `DateLancement`, `NomColl`, `Harmonie`) VALUES
(1, '2005-04-01', 'Marée Haute', 'Bleu'),
(2, '2005-04-15', 'Soleil', 'Jaune'),
(3, '2011-10-18', 'Vent Pourpre', 'Rouge');

-- --------------------------------------------------------

--
-- Structure de la table `COMMANDE`
--

CREATE TABLE IF NOT EXISTS `COMMANDE` (
  `BonCde` int(11) NOT NULL DEFAULT '0',
  `DateCde` date DEFAULT NULL,
  `NumCli` int(11) DEFAULT NULL,
  `TotalCde` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Contenu de la table `COMMANDE`
--

INSERT INTO `COMMANDE` (`BonCde`, `DateCde`, `NumCli`, `TotalCde`) VALUES
(200504637, '2005-09-15', 34067, 42),
(200504638, '2005-09-15', 35165, 61),
(200700500, '2015-06-12', 46840, 58),
(500200700, '2015-06-12', 45800, 38);

-- --------------------------------------------------------

--
-- Structure de la table `COM_ART`
--

CREATE TABLE IF NOT EXISTS `COM_ART` (
  `BonCde` int(11) NOT NULL DEFAULT '0',
  `RefArt` varchar(11) NOT NULL DEFAULT '',
  `Qte` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Structure de la table `consultation`
--

CREATE TABLE IF NOT EXISTS `consultation` (
  `numeroProprio` int(11) NOT NULL,
  `numeroAnimal` int(11) NOT NULL,
  `dateConsultation` date NOT NULL,
  `raison` varchar(20) NOT NULL,
  `prenomMedecin` varchar(20) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Contenu de la table `consultation`
--

INSERT INTO `consultation` (`numeroProprio`, `numeroAnimal`, `dateConsultation`, `raison`, `prenomMedecin`) VALUES
(1, 1, '2013-09-20', ' problème rénal', 'Gabrielle'),
(2, 2, '2013-09-20', ' vaccin', 'Gabrielle'),
(3, 3, '2013-09-03', 'plaie à la patte', 'Gabrielle'),
(4, 4, '2013-09-10', 'troubles du sommeil', 'Pierre'),
(5, 5, '2013-09-01', 'problèmes comporteme', 'Pierre');

-- --------------------------------------------------------

--
-- Structure de la table `consultation2`
--

CREATE TABLE IF NOT EXISTS `consultation2` (
`numeroC` int(11) NOT NULL,
  `numeroProprio` int(11) NOT NULL,
  `numeroAnimal` int(11) NOT NULL,
  `dateConsultation` date NOT NULL,
  `raison` varchar(20) NOT NULL,
  `prenomMedecin` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `consultation2`
--

INSERT INTO `consultation2` (`numeroC`, `numeroProprio`, `numeroAnimal`, `dateConsultation`, `raison`, `prenomMedecin`) VALUES
(16, 1, 1, '2013-09-20', 'problème rénal', 'Gabrielle'),
(17, 2, 2, '2013-09-20', 'vaccin', 'Gabrielle'),
(18, 3, 3, '2013-09-03', 'plaie à la patte', 'Gabrielle'),
(20, 4, 4, '2013-09-10', 'troubles du sommeil', 'Pierre'),
(21, 5, 5, '2013-09-01', 'probleme comporteme', 'Pierre');

-- --------------------------------------------------------

--
-- Structure de la table `contrat`
--

CREATE TABLE IF NOT EXISTS `contrat` (
`idContrat` int(11) NOT NULL,
  `idCli` int(11) NOT NULL,
  `idLogement` int(11) NOT NULL,
  `date` date NOT NULL,
  `montant` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `contrat`
--

INSERT INTO `contrat` (`idContrat`, `idCli`, `idLogement`, `date`, `montant`) VALUES
(1, 6, 4, '2011-12-30', 200),
(2, 3, 6, '2011-09-14', 250),
(3, 4, 6, '2011-11-17', 100),
(4, 11, 7, '2011-11-21', 120),
(5, 6, 3, '2011-11-05', 123),
(6, 8, 4, '2011-11-21', 206),
(7, 10, 5, '2011-11-19', 209),
(8, 1, 7, '2011-11-28', 301),
(9, 1, 7, '2011-11-11', 400),
(10, 10, 2, '2011-11-24', 800);

-- --------------------------------------------------------

--
-- Structure de la table `echantillon`
--

CREATE TABLE IF NOT EXISTS `echantillon` (
  `ECH_NUM` int(11) NOT NULL,
  `ECH_PRIXHT` double DEFAULT NULL,
  `MED_DEPOTLEGAL` varchar(9) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `famille`
--

CREATE TABLE IF NOT EXISTS `famille` (
  `FAM_CODE` varchar(3) NOT NULL,
  `FAM_LIBELLE` varchar(67) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `logement`
--

CREATE TABLE IF NOT EXISTS `logement` (
`idLogement` int(11) NOT NULL,
  `nbPiece` int(11) NOT NULL,
  `adresse` varchar(20) NOT NULL,
  `valeurEstimative` double NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `logement`
--

INSERT INTO `logement` (`idLogement`, `nbPiece`, `adresse`, `valeurEstimative`) VALUES
(1, 5, '29 rue du parc', 200000),
(2, 6, '17 rue du pommier', 400000),
(3, 24, '19 rue du chene', 1000000),
(4, 2, '17 rue du havre', 100000),
(5, 3, '15 rue du mistral', 130000),
(6, 8, '11 rue du chateigner', 600000),
(7, 5, '6 rue du chene', 100000);

-- --------------------------------------------------------

--
-- Structure de la table `medecin`
--

CREATE TABLE IF NOT EXISTS `medecin` (
`numMedecin` int(11) NOT NULL,
  `nomMedecin` varchar(20) NOT NULL,
  `prenomMedecin` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `medecin`
--

INSERT INTO `medecin` (`numMedecin`, `nomMedecin`, `prenomMedecin`) VALUES
(1, 'Burel', 'Gabrielle'),
(2, 'Burel', 'Pierre'),
(3, 'Parker', 'Peter'),
(4, 'Winchester', 'Dean'),
(5, 'Granger', 'Hermione');

-- --------------------------------------------------------

--
-- Structure de la table `medicament`
--

CREATE TABLE IF NOT EXISTS `medicament` (
  `MED_DEPOTLEGAL` varchar(9) NOT NULL,
  `MED_NOMCOMMERCIAL` varchar(19) DEFAULT NULL,
  `FAM_CODE` varchar(3) DEFAULT NULL,
  `MED_COMPOSITION` varchar(80) DEFAULT NULL,
  `MED_EFFETS` varchar(194) DEFAULT NULL,
  `MED_CONTREINDIC` varchar(236) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `proposer`
--

CREATE TABLE IF NOT EXISTS `proposer` (
  `V_NUM` int(11) NOT NULL,
  `ECH_NUM` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `proprietaire`
--

CREATE TABLE IF NOT EXISTS `proprietaire` (
`numProprio` int(11) NOT NULL,
  `nomProprio` varchar(20) NOT NULL,
  `prenomProprio` varchar(20) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `proprietaire`
--

INSERT INTO `proprietaire` (`numProprio`, `nomProprio`, `prenomProprio`) VALUES
(1, 'Castagnier', 'Aimable'),
(2, 'Castagnier', 'Aimable'),
(3, 'Aimée', 'Madeleine'),
(4, ' Brown', 'Charly'),
(5, ' Gargamel', 'Jules');

-- --------------------------------------------------------

--
-- Structure de la table `proprietaire2`
--

CREATE TABLE IF NOT EXISTS `proprietaire2` (
`numProprio` int(11) NOT NULL,
  `nomProprio` varchar(20) NOT NULL,
  `prenomProprio` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;

--
-- Contenu de la table `proprietaire2`
--

INSERT INTO `proprietaire2` (`numProprio`, `nomProprio`, `prenomProprio`) VALUES
(1, 'Castagnier', 'Aimable'),
(2, 'Castagnier', 'Aimable'),
(3, 'Aimée', 'Madeleine'),
(4, ' Brown', 'Charly'),
(5, ' Gargamel', 'Jules');

-- --------------------------------------------------------

--
-- Structure de la table `visite`
--

CREATE TABLE IF NOT EXISTS `visite` (
  `VIS_NUMSECU` varchar(4) DEFAULT NULL,
  `V_NUM` int(11) NOT NULL,
  `V_DESCRIPTION` varchar(90) DEFAULT NULL,
  `V_DATE` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure de la table `visiteur`
--

CREATE TABLE IF NOT EXISTS `visiteur` (
  `VIS_NUMSECU` varchar(4) NOT NULL,
  `VIS_NOM` varchar(15) DEFAULT NULL,
  `VIS_ADRESSE` varchar(31) DEFAULT NULL,
  `VIS_CP` varchar(5) DEFAULT NULL,
  `VIS_VILLE` varchar(19) DEFAULT NULL,
  `VIS_DATE_EMBAUCHE` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Index pour les tables exportées
--

--
-- Index pour la table `animal`
--
ALTER TABLE `animal`
 ADD PRIMARY KEY (`numAnimal`);

--
-- Index pour la table `animal2`
--
ALTER TABLE `animal2`
 ADD PRIMARY KEY (`numAnimal`);

--
-- Index pour la table `article`
--
ALTER TABLE `article`
 ADD PRIMARY KEY (`refprod`), ADD KEY `fk_NumColl` (`NumColl`);

--
-- Index pour la table `ARTICLE`
--
ALTER TABLE `ARTICLE`
 ADD PRIMARY KEY (`Refart`);

--
-- Index pour la table `client`
--
ALTER TABLE `client`
 ADD PRIMARY KEY (`idCli`);

--
-- Index pour la table `CLIENT`
--
ALTER TABLE `CLIENT`
 ADD PRIMARY KEY (`NumCli`);

--
-- Index pour la table `Collection`
--
ALTER TABLE `Collection`
 ADD PRIMARY KEY (`NumColl`);

--
-- Index pour la table `COMMANDE`
--
ALTER TABLE `COMMANDE`
 ADD PRIMARY KEY (`BonCde`), ADD KEY `fk_NumCli` (`NumCli`);

--
-- Index pour la table `COM_ART`
--
ALTER TABLE `COM_ART`
 ADD PRIMARY KEY (`BonCde`), ADD KEY `fk_RefArt` (`RefArt`);

--
-- Index pour la table `consultation`
--
ALTER TABLE `consultation`
 ADD PRIMARY KEY (`numeroProprio`,`numeroAnimal`), ADD KEY `consultation_ibfk_2` (`prenomMedecin`);

--
-- Index pour la table `consultation2`
--
ALTER TABLE `consultation2`
 ADD PRIMARY KEY (`numeroC`), ADD KEY `numeroAnimal` (`numeroAnimal`), ADD KEY `numeroProprio` (`numeroProprio`);

--
-- Index pour la table `contrat`
--
ALTER TABLE `contrat`
 ADD PRIMARY KEY (`idContrat`), ADD KEY `idCli` (`idCli`), ADD KEY `idLogement` (`idLogement`);

--
-- Index pour la table `echantillon`
--
ALTER TABLE `echantillon`
 ADD PRIMARY KEY (`ECH_NUM`), ADD KEY `MED_DEPOTLEGAL` (`MED_DEPOTLEGAL`);

--
-- Index pour la table `famille`
--
ALTER TABLE `famille`
 ADD PRIMARY KEY (`FAM_CODE`);

--
-- Index pour la table `logement`
--
ALTER TABLE `logement`
 ADD PRIMARY KEY (`idLogement`);

--
-- Index pour la table `medecin`
--
ALTER TABLE `medecin`
 ADD PRIMARY KEY (`numMedecin`);

--
-- Index pour la table `medicament`
--
ALTER TABLE `medicament`
 ADD PRIMARY KEY (`MED_DEPOTLEGAL`), ADD KEY `FAM_CODE` (`FAM_CODE`);

--
-- Index pour la table `proposer`
--
ALTER TABLE `proposer`
 ADD PRIMARY KEY (`V_NUM`,`ECH_NUM`), ADD KEY `ECH_NUM` (`ECH_NUM`);

--
-- Index pour la table `proprietaire`
--
ALTER TABLE `proprietaire`
 ADD PRIMARY KEY (`numProprio`);

--
-- Index pour la table `proprietaire2`
--
ALTER TABLE `proprietaire2`
 ADD PRIMARY KEY (`numProprio`);

--
-- Index pour la table `visite`
--
ALTER TABLE `visite`
 ADD PRIMARY KEY (`V_NUM`), ADD KEY `VIS_NUMSECU` (`VIS_NUMSECU`);

--
-- Index pour la table `visiteur`
--
ALTER TABLE `visiteur`
 ADD PRIMARY KEY (`VIS_NUMSECU`);

--
-- AUTO_INCREMENT pour les tables exportées
--

--
-- AUTO_INCREMENT pour la table `animal`
--
ALTER TABLE `animal`
MODIFY `numAnimal` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `animal2`
--
ALTER TABLE `animal2`
MODIFY `numAnimal` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `client`
--
ALTER TABLE `client`
MODIFY `idCli` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT pour la table `consultation2`
--
ALTER TABLE `consultation2`
MODIFY `numeroC` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT pour la table `contrat`
--
ALTER TABLE `contrat`
MODIFY `idContrat` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT pour la table `logement`
--
ALTER TABLE `logement`
MODIFY `idLogement` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT pour la table `medecin`
--
ALTER TABLE `medecin`
MODIFY `numMedecin` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `proprietaire`
--
ALTER TABLE `proprietaire`
MODIFY `numProprio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT pour la table `proprietaire2`
--
ALTER TABLE `proprietaire2`
MODIFY `numProprio` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=6;
--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `article`
--
ALTER TABLE `article`
ADD CONSTRAINT `fk_NumColl` FOREIGN KEY (`NumColl`) REFERENCES `Collection` (`NumColl`);

--
-- Contraintes pour la table `COMMANDE`
--
ALTER TABLE `COMMANDE`
ADD CONSTRAINT `fk_NumCli` FOREIGN KEY (`NumCli`) REFERENCES `CLIENT` (`NumCli`);

--
-- Contraintes pour la table `COM_ART`
--
ALTER TABLE `COM_ART`
ADD CONSTRAINT `fk_RefArt` FOREIGN KEY (`RefArt`) REFERENCES `ARTICLE` (`Refart`),
ADD CONSTRAINT `fk_BonCde` FOREIGN KEY (`BonCde`) REFERENCES `COMMANDE` (`BonCde`);

--
-- Contraintes pour la table `consultation2`
--
ALTER TABLE `consultation2`
ADD CONSTRAINT `consultation2_ibfk_2` FOREIGN KEY (`numeroAnimal`) REFERENCES `animal2` (`numAnimal`),
ADD CONSTRAINT `consultation2_ibfk_1` FOREIGN KEY (`numeroProprio`) REFERENCES `proprietaire2` (`numProprio`);

--
-- Contraintes pour la table `contrat`
--
ALTER TABLE `contrat`
ADD CONSTRAINT `contrat_ibfk_1` FOREIGN KEY (`idLogement`) REFERENCES `logement` (`idLogement`);

--
-- Contraintes pour la table `echantillon`
--
ALTER TABLE `echantillon`
ADD CONSTRAINT `echantillon_ibfk_1` FOREIGN KEY (`MED_DEPOTLEGAL`) REFERENCES `medicament` (`MED_DEPOTLEGAL`);

--
-- Contraintes pour la table `medicament`
--
ALTER TABLE `medicament`
ADD CONSTRAINT `medicament_ibfk_1` FOREIGN KEY (`FAM_CODE`) REFERENCES `famille` (`FAM_CODE`);

--
-- Contraintes pour la table `proposer`
--
ALTER TABLE `proposer`
ADD CONSTRAINT `proposer_ibfk_1` FOREIGN KEY (`V_NUM`) REFERENCES `visite` (`V_NUM`),
ADD CONSTRAINT `proposer_ibfk_2` FOREIGN KEY (`ECH_NUM`) REFERENCES `echantillon` (`ECH_NUM`);

--
-- Contraintes pour la table `visite`
--
ALTER TABLE `visite`
ADD CONSTRAINT `visite_ibfk_2` FOREIGN KEY (`VIS_NUMSECU`) REFERENCES `visiteur` (`VIS_NUMSECU`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
