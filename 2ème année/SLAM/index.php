<?php
require 'vendor/autoload.php';
use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;
//use \Firebase\JWT\JWT;

$app = new \Slim\App();

error_reporting(E_ALL);
ini_set('display_errors', 1);


$app->get('/gateaux', function(Request $request, Response $response){
    return getGateaux();
});

$app->get('/gateaux1', function(Request $request, Response $response){
    return getGateaux1();
});

$app->get('/gateaux2', function(Request $request, Response $response){
    return getGateaux2();
});

$app->get('/gateaux3', function(Request $request, Response $response){
    return getGateaux3();
});


$app->post('/gateau', function(Request $request, Response $response){
    //$gateau = json_decode($request->getBody());
    $jou_num = $request->getQueryParam('jou_num');
    $jou_nom = $request->getQueryParam('jou_nom');
    $jou_prenom = $request->getQueryParam('jou_prenom');
    $jou_mail = $request->getQueryParam('jou_mail');
    $jou_age = $request->getQueryParam('jou_age');
    $num_licence = $request->getQueryParam('num_licence');
    return setGateau($jou_num, $jou_nom, $jou_prenom, $jou_mail, $jou_age, $num_licence);
});


function connexion(){ 
        /*$vcap_services = json_decode($_ENV['VCAP_SERVICES'], true);
        $uri = $vcap_services['compose-for-mysql'][0]['credentials']['uri'];
        $db_creds = parse_url($uri);
        $dbname = "lafagl";
        $dsn = "mysql:host=" . $db_creds['host'] . ";port=" . $db_creds['port'] . ";dbname=" . $dbname;
        return $dbh = new PDO($dsn, $db_creds['user'], $db_creds['pass'],array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));*/
        return $dbh = new PDO("mysql:host=localhost;dbname=lafagl", 'root', 'root',array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8'));
     
}


function getGateaux()
{
    $sql = "SELECT * FROM Joueur";
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_CLASS); 
    return json_encode($result, JSON_PRETTY_PRINT);
 
}


function getGateaux1()
{
    $sql = "SELECT * FROM Parent";
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_CLASS); 
    return json_encode($result, JSON_PRETTY_PRINT);
 
}


function getGateaux2()
{
    $sql = "SELECT * FROM Licence";
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_CLASS); 
    return json_encode($result, JSON_PRETTY_PRINT);
 
}

function getGateaux3()
{
    $sql = "SELECT * FROM Finance";
    $dbh=connexion();
    $statement = $dbh->prepare($sql);
    $statement->execute();
    $result = $statement->fetchAll(PDO::FETCH_CLASS); 
    return json_encode($result, JSON_PRETTY_PRINT);
 
}


function setGateau($jou_num, $jou_nom, $jou_prenom, $jou_mail, $jou_age, $num_licence)
{
    $dbh=connexion(); 
    $req=$dbh->prepare('INSERT INTO Joueur VALUES (:jou_num, :jou_nom, :jou_prenom, :jou_mail, :jou_age, :num_licence)');
    $res=$req->execute(array(
        ':jou_num'=> $jou_num,
        ':jou_nom'=> $jou_nom,
        ':jou_prenom'=> $jou_prenom,
        ':jou_mail'=> $jou_mail,
        ':jou_age'=> $jou_age,
        ':num_licence'=> $num_licence,
       
    ));
    return $res;
}


  
$app->run();

?>