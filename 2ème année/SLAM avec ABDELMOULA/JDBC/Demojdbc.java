package com.dem.demoJdbc;

import java.sql.*;

public class Demojdbc {

	public static void main(String[] args) throws SQLException {
		// TODO Auto-generated method stub
		Demojdbc.sauverEnBase("LL");

	}
	public static void sauverEnBase (String n) throws SQLException{
		String url ="jdbc:mysql://neotri.intern/lafagl";
		String login = "lafagl";
		String passwd = "";
		Connection cn =null;
		Statement st =null;
		
		try{
			//Etape 1 : Chargement du driver
			Class.forName("com.mysql.jdbc.Driver");
			//Etape 2 : RÈcupÈration de la connexion
			cn = (Connection) DriverManager.getConnection(url, login, passwd);
			//Etape 3 : Création d'un statement
			st = (Statement) cn.createStatement();
			//requete pour insert dans la base de donnée
			//String sql = "INSERT INTO `User` (`Nom`,`Age` ) VALUES ('Julie','17')";
			
		

			//Etape 4 : Exécution requete
			//int i = st.executeUpdate(sql);
			//if(i==1){
				//System.out.println("ok");
			//}
			
			//requete et execution pour afficher la base de donnée
			String reqSelect = "SELECT * from `User`";
			 ResultSet rs = st.executeQuery(reqSelect);
			 while (rs.next()){
					String nom = rs.getString(1);
					int age = rs.getInt(2);
					System.out.println(nom + " : "+ age);
					
				}

			 //Autre moyen d'inserer dans une base de donnée 
			String reqInsert2= "INSERT INTO User values(?,?) ";
				PreparedStatement pstmt = cn.prepareStatement(reqInsert2);
				pstmt.setString(1,"LV");
				pstmt.setInt(2,19);
				pstmt.executeUpdate();
			
			
			
			
		}
		catch (SQLException e){
			e.printStackTrace();
		}catch (ClassNotFoundException e){
			e.printStackTrace();
		}finally{
			try{
			//Etape 5 : Libérer ressources de la mémoire
				cn.close();
				st.close();
			}catch(SQLException e){
				e.printStackTrace();
			}
		}
		}
		
	}
			
			
		
			



		
