package com.demorest;

import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.Produces;
import javax.ws.rs.PathParam;
import javax.ws.rs.core.MediaType;

@Path("/user/{nom}")
public class HelloNom {

    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getMessage(@PathParam("nom") String nom) {

        return "Hello "+nom;

    }

}
