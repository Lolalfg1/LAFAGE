package com.demorest;
import java.util.List;

public class DaoTest {

    public static void main(String [] args) {
        UserJDBC dao = new UserJDBC();
        //Methode Select
        List<User> users = dao.getAllUsers();
        for (User user : users) {
            System.out.print(" L'utilisateur se nomme " + user.getNom() + " et il a " + user.getAge() + " ans.");
        }

        //Methode Select by nom
        User us = new User("Liv", 17);
        dao.SelectUser(us);

        //Methode Insert
        //User u = new User("Claire", 20);
        //dao.InsertUser(u);

        //Methode Delete
        //User use = new User("Lucie", 17);
        //dao.DeleteUser(use);

}

}
