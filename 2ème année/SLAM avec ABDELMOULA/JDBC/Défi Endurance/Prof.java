public class Prof {

    private int idProf;
    private String nomProf;
    private String nomClasse;
    private String nomEcole;
    private Classe c;

    public int getIdProf() {
        return idProf;
    }

    public void setIdProf(int idProf) {
        this.idProf = idProf;
    }

    public String getNomProf() {
        return nomProf;
    }

    public void setNomProf(String nomProf) {
        this.nomProf = nomProf;
    }

    public String getNomClasse() {
        return nomClasse;
    }

    public void setNomClasse(String nomClasse) {
        this.nomClasse = nomClasse;
    }

    public String getNomEcole() {
        return nomEcole;
    }

    public void setNomEcole(String nomEcole) {
        this.nomEcole = nomEcole;
    }

    public Classe getC() {
        return c;
    }

    public void setC(Classe c) {
        this.c = c;
    }

    public Prof(int idProf, String nomProf, String nomClasse, String nomEcole, Classe c) {
        this.idProf = idProf;
        this.nomProf = nomProf;
        this.nomClasse = nomClasse;
        this.nomEcole = nomEcole;
        this.c = c;
    }

    @Override
    public String toString() {
        return "Prof{" +
                "idProf=" + idProf +
                ", nomProf='" + nomProf + '\'' +
                ", nomClasse='" + nomClasse + '\'' +
                ", nomEcole='" + nomEcole + '\'' +
                ", c=" + c +
                '}';
    }
}
