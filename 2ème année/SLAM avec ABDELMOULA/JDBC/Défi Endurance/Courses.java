public class Courses {

    private int idCourses;
    private double objectif;
    private double distanceFaite;
    private Classe c;


    public int getIdCourses() {
        return idCourses;
    }

    public void setIdCourses(int idCourses) {
        this.idCourses = idCourses;
    }

    public double getObjectif() {
        return objectif;
    }

    public void setObjectif(double objectif) {
        this.objectif = objectif;
    }

    public double getDistanceFaite() {
        return distanceFaite;
    }

    public void setDistanceFaite(double distanceFaite) {
        this.distanceFaite = distanceFaite;
    }

    public Classe getC() {
        return c;
    }

    public void setC(Classe c) {
        this.c = c;
    }

    public Courses(int idCourses, double objectif, double distanceFaite, Classe c) {
        this.idCourses = idCourses;
        this.objectif = objectif;
        this.distanceFaite = distanceFaite;
        this.c = c;
    }

    @Override
    public String toString() {
        return "Courses{" +
                "idCourses=" + idCourses +
                ", objectif=" + objectif +
                ", distanceFaite=" + distanceFaite +
                ", c=" + c +
                '}';
    }
}
