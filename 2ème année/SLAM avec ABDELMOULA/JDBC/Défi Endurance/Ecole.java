public class Ecole {

    private int idEcole;
    private int nbClasse;
    private String nomEcole;

    public int getIdEcole() {
        return idEcole;
    }

    public void setIdEcole(int idEcole) {
        this.idEcole = idEcole;
    }

    public int getNbClasse() {
        return nbClasse;
    }

    public void setNbClasse(int nbClasse) {
        this.nbClasse = nbClasse;
    }

    public String getNomEcole() {
        return nomEcole;
    }

    public void setNomEcole(String nomEcole) {
        this.nomEcole = nomEcole;
    }

    public Ecole(int idEcole, int nbClasse, String nomEcole) {
        this.idEcole = idEcole;
        this.nbClasse = nbClasse;
        this.nomEcole = nomEcole;
    }

    @Override
    public String toString() {
        return "Ecole{" +
                "idEcole=" + idEcole +
                ", nbClasse=" + nbClasse +
                ", nomEcole='" + nomEcole + '\'' +
                '}';
    }
}
