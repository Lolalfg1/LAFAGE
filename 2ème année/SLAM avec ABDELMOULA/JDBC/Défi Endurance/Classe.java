public class Classe {

    private int idClasse;
    private String nomProf;
    private int nbEleve;
    private Ecole e;

    public int getIdClasse() {
        return idClasse;
    }

    public void setIdClasse(int idClasse) {
        this.idClasse = idClasse;
    }

    public String getNomProf() {
        return nomProf;
    }

    public void setNomProf(String nomProf) {
        this.nomProf = nomProf;
    }

    public int getNbEleve() {
        return nbEleve;
    }

    public void setNbEleve(int nbEleve) {
        this.nbEleve = nbEleve;
    }

    public Ecole getE() {
        return e;
    }

    public void setE(Ecole e) {
        this.e = e;
    }

    public Classe(int idClasse, String nomProf, int nbEleve, Ecole e) {
        this.idClasse = idClasse;
        this.nomProf = nomProf;
        this.nbEleve = nbEleve;
        this.e = e;
    }

    @Override
    public String toString() {
        return "Classe{" +
                "idClasse=" + idClasse +
                ", nomProf='" + nomProf + '\'' +
                ", nbEleve=" + nbEleve +
                ", e=" + e +
                '}';
    }
}
