package com.demorest;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.demorest.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

@Path("/users")
public class UserService {
    ArrayList<User> users = new ArrayList<User>();




    public UserService(){
        users.add(new User("Lola", 19));
        users.add(new User("Lucie", 20));
        users.add(new User("Samuel", 14));



    }
@GET
@Produces(MediaType.APPLICATION_JSON)
public String getUsers() {
    //Affiche les Noms et les Ages des utilisateurs.
    ObjectMapper mapper = new ObjectMapper();
    String jsonInString = null;
    try {
        jsonInString = mapper.writeValueAsString(users);
    } catch (Exception e) {
        return e.getMessage();
    }
    return jsonInString;
}
@GET
@Path("/{nom}")
@Produces(MediaType.APPLICATION_JSON)
public String getUser(@PathParam("nom") String nom) {
    User res=null;
    ObjectMapper mapper = new ObjectMapper();

    for(User u : users)
    {
        if(u.getNom().equalsIgnoreCase(nom)) res=u;
    }
    String jsonInString = null;
    try {
        jsonInString = mapper.writeValueAsString(res);
    } catch (Exception e) {
        return e.getMessage();
    }
    return jsonInString;
}

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    public String addUser(@FormParam("nom") String nom, @FormParam("age") int age) {
        //code html formulaire pour la saisie du nom

        User res=null;
        ObjectMapper mapper = new ObjectMapper();

        User u = new User(nom, age);
        users.add(u);
        String jsonInString = null;
        try {
            jsonInString = mapper.writeValueAsString(users);
        } catch (Exception e) {
            return e.getMessage();
        }
        return jsonInString;
    }

    //Supprime un utilisateur
    @DELETE
    @Path("/{nom}")
    @Produces(MediaType.APPLICATION_JSON)
    public String suppUser(@PathParam("nom") String nom) {
        User res=null;
        ObjectMapper mapper = new ObjectMapper();
        for(User u : users)
        {
            if(u.getNom().equalsIgnoreCase(nom)) res=u;
        }

        users.remove(res);
        String jsonInString = null;
        try {
            jsonInString = mapper.writeValueAsString(users);
        } catch (Exception e) {
            return e.getMessage();
        }
        return jsonInString;
    }

//Ajoute un utilisateur
    @PUT
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String addUser(User user) {
        ObjectMapper mapper = new ObjectMapper();
        String jsonInString = null;
        users.add(user);
        try {
            jsonInString = mapper.writeValueAsString(users);
        } catch (Exception e) {
            return e.getMessage();
        }
        return jsonInString;
    }
}