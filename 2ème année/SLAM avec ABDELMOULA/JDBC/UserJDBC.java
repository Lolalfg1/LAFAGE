package com.demorest;

import javax.ws.rs.Path;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.PreparedStatement;
import java.util.ArrayList;
import java.util.List;


public class UserJDBC {
    public Connection getConnection() {
        Connection conn = null;


        try {

            Class.forName("com.mysql.jdbc.Driver");

            System.out.println("Oki");

            String url = "jdbc:mysql://neotri.intern/lafagl?useUnicode=true&useJDBCCompliantTimezoneShift=true&useLegacyDatetimeCode=false&serverTimezone=UTC";

            String user = "lafagl";

            String passwd = "";

            conn = DriverManager.getConnection(url, user, passwd);


        } catch (Exception e) {

            e.printStackTrace();

        }

        return conn;


    }

    //Methode Select
    public List<User> getAllUsers(){

        ArrayList<User> userList = new ArrayList<User>();
        try{
            Connection conn = this.getConnection();
            Statement statement = conn.createStatement();

            ResultSet resultat = statement.executeQuery("SELECT * FROM lafagl.User;");

            while( resultat.next() ){
                User user1 = new User();
                user1.setAge(resultat.getInt("age"));
                user1.setNom(resultat.getString("nom"));
                userList.add(user1);
            }

        }catch(Exception e){

            e.printStackTrace();

        }

            return userList;

        }

    //Methode Select by nom
    public void SelectUser(User us){

        ArrayList<User> userList4 = new ArrayList<User>();
        try{
            Connection conn = this.getConnection();
            Statement statement = conn.createStatement();


            PreparedStatement pstmt3 = conn.prepareStatement("SELECT Age from lafagl.User where Nom=?;");
            //pstmt3.setString(1, us.getNom());
            pstmt3.setInt(2, us.getAge());
            int ag = pstmt3.executeUpdate();

            if(ag==1){
                System.out.println("ça roule!!!");
            }

        }catch(Exception e){

            e.printStackTrace();

        }

    }

    //Methode Insert
    public void InsertUser(User u){

        ArrayList<User> userList1 = new ArrayList<User>();
        try{
            Connection conn = this.getConnection();
            Statement statement = conn.createStatement();


            PreparedStatement pstmt = conn.prepareStatement("INSERT INTO lafagl.User VALUES (?,?)");
            pstmt.setString(1, u.getNom());
            pstmt.setInt(2, u.getAge());
            int i = pstmt.executeUpdate();


            if(i==1){
            System.out.println("Dacc, c'est fait !!!");
            }

        }catch(Exception e){

            e.printStackTrace();

        }

    }

    //Methode Delete
    public void DeleteUser(User use){

        ArrayList<User> userList2 = new ArrayList<User>();
        try{
            Connection conn = this.getConnection();
            Statement statement = conn.createStatement();


            PreparedStatement pstmt2 = conn.prepareStatement("DELETE from lafagl.User where Nom=? and Age=?;" );
            pstmt2.setString(1, use.getNom());
            pstmt2.setInt(2, use.getAge());
            int a = pstmt2.executeUpdate();


            if(a==1){
                System.out.println("Dacc, c'est fait, il est supp !!!");
            }

        }catch(Exception e){

            e.printStackTrace();

        }

    }






}
