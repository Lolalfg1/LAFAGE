package com.HelloServlet.servlets;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;

@WebServlet(name = "Connexion")
public class Connexion extends HttpServlet {

    @Override
    public void init() throws ServletException {
        // Servlet initialization code here
        super.init();
    }

    @Override
    public void destroy() {
        // resource release
        super.destroy();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // Set response content type
        response.setContentType("text/html");

        //creation de l'objet utilisé pour l'affichage
        PrintWriter out = response.getWriter();
      

        //code html formulaire pour la saisie du nom
        out.println("<form method='POST' action=''>" +
                "nom: <input name=nom type='text' id='nom'>" +
                "<INPUT type=submit value=Envoyer>" +
                "</form>");


    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        response.getContentType();

        PrintWriter out = response.getWriter();

        //récupération du paramètre du formulaire et affichage
        String nom = request.getParameter("nom");

        try {

            Class.forName("com.mysql.jdbc.Driver");

            System.out.println("ok");
            //connection base de donnée
            String url = "jdbc:mysql://neotri.intern/lafagl";
            String user = "lafagl";
            String passwd = "";


            Connection cn = DriverManager.getConnection(url, user, passwd);
            Statement st = cn.createStatement();
            //requete sql
            ResultSet resul = st.executeQuery("SELECT Age from lafagl.User where Nom='"+nom+"'");

            while( resul.next() ){
                int age = resul.getInt("age");
                out.println(age);
            }

        }catch(Exception e){

            e.printStackTrace();

        }
    }

}
