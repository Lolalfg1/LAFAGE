package com.HelloServlet.servlets;
import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

public class HelloServlet extends HttpServlet {

	

	public void doGet(HttpServletRequest req, HttpServletResponse res) throws ServletException, IOException {
		res.setContentType("text/html");
		PrintWriter out = res.getWriter();
		String nom = req.getParameter("nom");
		String age = req.getParameter("age");
		
		
		out.println("<html>");
		out.println("<head><title>Bonjour "+ nom +" vous êtes "+ age +"</title></head>");
		out.println("<body>");
		out.println("Bonjour " + nom + "!" + " vous avez "+ age + " ans,");
		out.println("</body></html>");
		
		int a = Integer.parseInt(age);
		if (a<18){
			out.println(" donc vous êtes mineur.");
		}else{
			out.println(" donc vous êtes majeur.");
		}
	}
	
	public String getServletInfo(){
		return "Servlet dit son nom" + "bonjour" + "Servlet dit son age";
	
		
	}
	
	
	
	
}
